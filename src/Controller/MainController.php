<?php

namespace App\Controller;

use App\Entity\Todo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MainController extends AbstractController
{
    /**
     * @Route(path="/")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {

        $todo = new Todo();

        $entity = $this->getDoctrine()->getRepository(Todo::class);

        if(empty($this->getUser())) {
            return $this->redirectToRoute('login');
        }
        $todo = $entity->findBy([
            'status' => 1,
            'userId' => $this->getUser()->id
        ]);

        return $this->render('Main/index.html.twig', [
            'todo' => $todo,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route(path="/register")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request): Response
    {
        $request = Request::createFromGlobals();
        $status = 0;
        $message = '';
        if(!empty($request->request->get('submit'))) {
            if(!empty($request->request->get('login'))) {
                $login = $request->request->get('login');
            }
            if(!empty($request->request->get('email'))) {
                $email = $request->request->get('email');
            }
            if(!empty($request->request->get('password'))) {
                $password = $request->request->get('password');
            }
            if(!empty($request->request->get('password2'))) {
                $password2 = $request->request->get('password2');
            }
            if(!empty($login) && !empty($email) && !empty($password) && !empty($password2)) {
                $status = 1;
                if (strlen($login) <= 5) {
                    $message = 'Login musi zawierać co najmniej 6 znaków!';
                    $status = 2;
                } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $message = 'Podany e-mail jest błędny!';
                    $status = 2;
                } else if (strlen($password) <= 7) {
                    $message = 'Hasło musi zawierać co najmniej 8 znaków!';
                    $status = 2;
                } else if (strlen($password) >= 50) {
                    $message = 'Hasło musi zawierać maksymalnie 49 znaków!';
                    $status = 2;
                } else if (!preg_match("/\d/", $password)) {
                    $message = 'Hasło musi zawierać co najmniej jedną cyfrę!';
                    $status = 2;
                } else if (!preg_match("/[A-Z]/", $password)) {
                    $message = 'Hasło musi zawierać co najmniej jedną dużą literę!';
                    $status = 2;
                } else if (!preg_match("/[a-z]/", $password)) {
                    $message = 'Hasło musi zawierać co najmniej jedną małą literę!';
                    $status = 2;
                } else if (!preg_match("/\W/", $password)) {
                    $message = 'Hasło musi zawierać co najmniej jeden znak specjalny!';
                    $status = 2;
                } else if (preg_match("/\s/", $password)) {
                    $message = 'Hasło nie może zawierać spacji!';
                    $status = 2;
                } else if ($password != $password2) {
                    $message = 'Hasła muszą być takie same!';
                    $status = 2;
                }
                if($status == 1) {
                    $password = hash('sha256', $password);
                    $conn = $this->getDoctrine()->getManager()->getConnection();

                    $query2 = "SELECT count(*) FROM `users` WHERE `login` = '$login' OR `email` = '$email' LIMIT 1;";
                    $statement2 = $conn->prepare($query2);
                    $statement2->execute();
                    if($statement2->fetchOne() == 0) {
                        $query = "INSERT INTO users (`login`, `email`, `password`, `group`) VALUES ('$login', '$email', '$password', 'USER');";
                        $statement = $conn->prepare($query);
                        $statement->execute();

                        $message = 'Pomyślnie zarejestrowano! Możesz się teraz zalogować!';
                    } else {
                        $status = 2;
                        $message = 'Podany email lub login jest już zajęty';
                    }

                }
            }
        }
        return $this->render('Main/register.html.twig', [
            'status' => $status,
            'message' => $message
        ]);
    }

}