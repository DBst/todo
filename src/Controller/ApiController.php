<?php

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /**
     * @Route("/api")
     * @return JsonResponse
     */
    public function index()
    {
        return $this->json([
            'message' => 'Witaj w API!'
        ]);
    }

    /**
     * @Route("/api/add-record")
     */
    public function addRecord()
    {
        $request = Request::createFromGlobals();
        if(!empty($request->request->get('desc'))) {
            $desc = $request->request->get('desc');
            $date = date('Y-m-d H:i:s');
            $user_id = $this->getUser()->id;

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $query = "INSERT INTO todo (`desc`, `date`, status, user_id) VALUES ('$desc', '$date', '1', '$user_id');";
            $statement = $conn->prepare($query);
            $statement->execute();

            $query2 = "SELECT id FROM `todo` WHERE `user_id` = $user_id ORDER BY `date` DESC LIMIT 1;";
            $statement2 = $conn->prepare($query2);
            $statement2->execute();

            return $this->json([
                'status' => true,
                'data' => [
                    'id' => $statement2->fetchOne()
                ]
            ]);
        }
        return $this->json([
            'status' => false
        ]);
    }

    /**
     * @Route("/api/update-record")
     */
    public function updateRecord()
    {
        $request = Request::createFromGlobals();
        if(!empty($request->request->get('id')) && !empty($request->request->get('desc'))) {
            $id = $request->request->get('id');
            $desc = $request->request->get('desc');
            $user_id = $this->getUser()->id;

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $query = "UPDATE `todo` SET `desc` = '$desc' WHERE `todo`.`id` = $id AND `user_id` = $user_id;";
            $statement = $conn->prepare($query);
            $statement->execute();

            return $this->json([
                'status' => true
            ]);
        }
        return $this->json([
            'status' => false
        ]);
    }

    /**
     * @Route("/api/remove-record")
     */
    public function removeRecord()
    {
        $request = Request::createFromGlobals();
        if(!empty($request->request->get('id'))) {
            $id = $request->request->get('id');
            $user_id = $this->getUser()->id;

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $query = "UPDATE `todo` SET `status` = '2' WHERE `todo`.`id` = $id AND `user_id` = $user_id;";
            $statement = $conn->prepare($query);
            $statement->execute();

            return $this->json([
                'status' => true
            ]);
        }
        return $this->json([
            'status' => false
        ]);
    }
}