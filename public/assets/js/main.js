
    $(document).ready(function(){
       init();
    });

    function init() {
        $('.remove-button').on('click', function () {
            let id = $(this).attr('name').split("-")[1];
            AJAXrequest('/api/remove-record', 'id=' + id, function (data = null) {
                $('#todo_id-' + id).remove();
            })
        });
        $('.update-button').on('click', function () {
            let id = $(this).attr('name').split("-")[1];
            let desc = $('.todo_value-' + id).val();
            AJAXrequest('/api/update-record', 'id=' + id + '&desc=' + desc, function (data = null) {
                console.log('sukces');
            })
        });
    }

    $('#todo_add input[type="button"]').on('click', function(){
        let desc = $('#todo_add input[type="text"]').val();
        AJAXrequest('/api/add-record', "&desc=" + desc, function(data = null){
            let id = data['id'];
            $('.todo-list').append(`
                <form id="todo_id-` + id + `">
                    <input type="text" class="todo_value-` + id + `" name="todo_value-` + id + `" value="` + desc + `">
                    <input type="button" class="update-button" name="todo_update-` + id + `" value="Zapisz">
                    <input type="button" class="remove-button" name="todo_remove-` + id + `" value="Usuń">
                </form>
            `);
            $('#todo_add input[type="text"]').val('');
            init();
        })
    });

    function AJAXrequest(url, data, callback) {

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function(Response) {
                if(typeof Response != null) {
                    if (Response['status'] == true) {
                        if(typeof Response['data'] != null) {
                            callback(Response['data']);
                        } else {
                            callback(Response);
                        }
                    }
                }
            }
        });

    }
